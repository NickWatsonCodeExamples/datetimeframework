//
//  DateTimeFramework.h
//  DateTimeFramework
//
//  Created by Nick Watson on 21/06/2018.
//  Copyright © 2018 Nick Watson. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for DateTimeFramework.
FOUNDATION_EXPORT double DateTimeFrameworkVersionNumber;

//! Project version string for DateTimeFramework.
FOUNDATION_EXPORT const unsigned char DateTimeFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <DateTimeFramework/PublicHeader.h>


