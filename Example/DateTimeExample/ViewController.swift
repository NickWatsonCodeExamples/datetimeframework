//
//  ViewController.swift
//  DateTimeExample
//
//  Created by Nick Watson on 21/06/2018.
//  Copyright © 2018 Nick Watson. All rights reserved.
//

import UIKit
import DateTimeFramework

class ViewController: UIViewController, DateTimeSelectorDelegate {
    var picker: DateTimeSelector?
    
    @IBOutlet weak var pickedDate:UILabel!
    
    @IBAction func showDateTimePicker(sender: AnyObject) {
        let min = Date().addingTimeInterval(0)
        let max = Date().addingTimeInterval(120 * 160 * 124 * 14)
        let picker = DateTimeSelector.show(selected: Date(), minimumDate: min, maximumDate: max)
        picker.timeInterval = DateTimeSelector.MinuteInterval.default
        picker.highlightColor = UIColor(red: 7.0/255.0, green: 105.0/255.0, blue: 112.0/255.0, alpha: 1)
        picker.darkColor = UIColor.darkGray
        picker.doneButtonTitle = "Select"
        picker.doneBackgroundColor = UIColor(red: 7.0/255.0, green: 105.0/255.0, blue: 112.0/255.0, alpha: 1)
        picker.locale = Locale(identifier: "en_GB")
        
        picker.todayButtonTitle = "Today"
        picker.is12HourFormat = false
        picker.dateFormat = "hh:mm aa dd/MM/YYYY"
        picker.isTimePickerOnly = false
        picker.includeMonth = true // if true the month shows at top
        picker.completionHandler = { date in
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm aa dd/MM/YYYY"
            self.pickedDate.text = formatter.string(from: date)
        }
        picker.delegate = self
        self.picker = picker
    }
    
    func dateTimeSelector(_ picker: DateTimeSelector, didSelectDate: Date) {
        // do stuff with selected picker.selectedDate
        
    }
}


